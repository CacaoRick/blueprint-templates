import React from 'react'
import PropTypes from 'prop-types'

import styles from './style.module.scss'

export default class {{pascalCase name}} extends React.Component {
  static propTypes = {
    // children: PropTypes.any,
  }

  render () {
    return (
      <div className={ styles.{{camelCase name}} }>
        <div>{'Hello {{pascalCase name}}'}</div>
      </div>
    )
  }
}
