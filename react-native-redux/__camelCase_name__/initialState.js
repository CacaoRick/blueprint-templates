import Immutable from 'immutable'

/** @type {Immutable.Map<string, {{pascalCase name}}State>} {{pascalCase name}}StateMap */
export const initial{{pascalCase name}}State = Immutable.fromJS({

})

export default initial{{pascalCase name}}State
