import { fromJS } from 'immutable'
import Types from '../ActionTypes'
import initialState from './initialState'

export default (state = initialState, action) => {
  switch (action.type) {
    case Types.ACTION_TYPE: {
      return state
        .set('', fromJS(action.payload))
      }
    case Types.ACTION_TYPE: {
      return state
        .setIn(['', ''], fromJS(action.payload))
    }
    default: {
      return state
    }
  }
}
