import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import actions from '../../redux/actions'
import styles from './style.module.scss'

class {{pascalCase name}} extends React.Component {
  static propTypes = {
    // prop: PropTypes.any,
  }

  render () {
    return (
      <div className={ styles.{{camelCase name}} }>
        {'Hello {{pascalCase name}}'}
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    // data: state.getIn(['reducer', 'data']),
  }
}

const mapDispatchToProps = {
  // action: actions.action,
}

export default connect(mapStateToProps, mapDispatchToProps)({{pascalCase name}})
