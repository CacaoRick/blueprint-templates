import React from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, View, Text } from 'react-native'

/**
 * @typedef {Object} Props
*/

/**
 * @typedef {Object} State
*/

/** @extends {React.Component<Props, State>} */
export default class {{pascalCase name}} extends React.Component {
  static propTypes = {
    // prop: PropTypes.any,
  }

  render () {
    return (
      <View style={styles.container}>
        <Text>{'Hello {{pascalCase name}}'}</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center'
  }
})
